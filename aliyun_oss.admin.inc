<?php

/**
 * Settings form.
 */
function aliyun_oss_settings_form($form, &$form_state) {
  $form['required'] = [
    '#type' => 'fieldset',
    '#title' => t('Aliyun OSS Bucket Info'),
  ];
  $form['required']['aliyun_oss_key_id'] = [
    '#type' => 'textfield',
    '#title' => t('Aliyun OSS Access Key ID'),
    '#default_value' => variable_get('aliyun_oss_key_id', ''),
    '#required' => TRUE,
  ];
  $form['required']['aliyun_oss_key_secret'] = [
    '#type' => 'textfield',
    '#title' => t('Aliyun OSS Access Key Secret'),
    '#default_value' => variable_get('aliyun_oss_key_secret', ''),
    '#required' => TRUE,
  ];
  $form['required']['aliyun_oss_bucket'] = [
    '#type' => 'textfield',
    '#title' => t('Aliyun Bucket ID'),
    '#default_value' => variable_get('aliyun_oss_bucket', ''),
    '#description' => t('即 Bucket 名'),
    '#required' => TRUE,
  ];
  $form['required']['aliyun_oss_bucket_type'] = [
    '#type' => 'select',
    '#title' => t('Aliyun Bucket 类型'),
    '#default_value' => variable_get('aliyun_oss_bucket_type', 'type_private'),
    '#options' => [
      'type_private' => '私有',
      'type_protected' => '公共读',
      'type_public' => '公共读写',
    ],
    '#required' => TRUE,
    '#chosen' => TRUE,
  ];

  $form['required']['aliyun_oss_endpoint'] = [
    '#type' => 'textfield',
    '#title' => t('Aliyun Endpoint'),
    '#default_value' => variable_get('aliyun_oss_endpoint', ''),
    '#required' => TRUE,
    '#description' => t('<a target="_blank" href="@href">点击查看各个区域 Endpoint 地址</a>', ['@href' => 'https://help.aliyun.com/document_detail/31837.html']),
  ];

  $form['cname'] = [
    '#type' => 'fieldset',
    '#title' => t('Aliyun OSS CNAME Info'),
  ];
  $form['cname']['aliyun_oss_cname'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable CNAME support'),
    '#description' => t('Enable it if you want to use CNAME with OSS'),
    '#default_value' => variable_get('aliyun_oss_cname', 0),
  ];
  $form['cname']['aliyun_oss_domain'] = [
    '#type' => 'textfield',
    '#title' => t('CNAME Address'),
    '#default_value' => variable_get('aliyun_oss_domain', ''),
  ];

  $form['aliyun_oss_cache'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable database caching'),
    '#description' => t('Enable a local file metadata cache, this significantly reduces calls to OSS'),
    '#default_value' => variable_get('aliyun_oss_cache', 1),
  ];

  return system_settings_form($form);
}
